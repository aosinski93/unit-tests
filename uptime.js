const os = require('os');
const formatDate = require('./index.js');

const uptime = formatDate(77820);

console.log(`Current uptime is ${uptime}`);