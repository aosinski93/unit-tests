const formatDate = (timeInSeconds) => {
    if (!timeInSeconds || timeInSeconds === 0) {
        return '0s';
    }

    const hours = Math.floor(timeInSeconds / 3600);
    const minutes = Math.floor((timeInSeconds - (hours * 3600)) / 60);
    const seconds = timeInSeconds % 60;
    
    if (hours === 0) {       
        if( minutes === 0 ) {
            return `${seconds}s`
        } 
        else if (seconds === 0) {
           return `${minutes}m`;
        }
        return `${minutes}m ${seconds}s`;
        
    }
    if (minutes === 0) {
        if( hours === 0 ) {
            return `${seconds}s`
        } 
        if (minutes === 0 && seconds === 0) {
            return `${hours}h`;
        }
        return `${hours}h ${seconds}s`;
    }
    if (seconds === 0) {
        if( hours === 0 ) {
            return `${minutes}s`
        } 
        return `${hours}h ${minutes}m`;
    }


    return `${hours}h ${minutes}m ${seconds}s`;   

}

module.exports = formatDate;